package es.urjc.code.practice;

import es.urjc.code.practice.services.LoggerService;
import es.urjc.code.practice.services.MockServiceFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles(value = {"h2"})
class DataLoaderTest {

    private DataLoader dataLoader;
    @Autowired
    private LoggerService loggerService;
    @Autowired
    private MockServiceFactory mockServiceFactory;

    @BeforeEach
    void setUp() {
        this.dataLoader = new DataLoader(this.mockServiceFactory.getH2PlaneService(),
                this.mockServiceFactory.getH2CabinCrewService(),
                this.loggerService);
    }

    @Test
    void shouldRun() {
        this.dataLoader.run();
    }
}
