package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.FlatPlaneWithMechanicDTO;
import es.urjc.code.practice.dtos.MechanicDTO;
import es.urjc.code.practice.dtos.PlaneDTO;
import es.urjc.code.practice.models.Plane;
import es.urjc.code.practice.repositories.PlaneRepository;
import es.urjc.code.practice.services.impl.PlaneServiceImpl;
import es.urjc.code.practice.utils.EntitiesLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.util.CollectionUtils.isEmpty;


@SpringBootTest
@ActiveProfiles(value = {"h2"})
class PlaneServiceMockTest {

    private PlaneService planeService;
    @Autowired
    private PlaneRepository planeRepository;
    @Autowired
    private EntitiesLoader prepareDatabase;

    @BeforeEach
    void setUp() throws ParseException {
        this.prepareDatabase.setUp();
        this.planeService = new PlaneServiceImpl(new PlaneRepository() {
            @Override
            public List<FlatPlaneWithMechanicDTO> findPlaneMechanics() {
                return planeRepository.findPlaneMechanics();
            }

            @Override
            public List<FlatPlaneWithMechanicDTO> findPlaneMechanicsJSON() {
                return planeRepository.findPlaneMechanics();
            }

            @Override
            public List<Plane> findAll() {
                return null;
            }

            @Override
            public List<Plane> findAll(Sort sort) {
                return null;
            }

            @Override
            public List<Plane> findAllById(Iterable<Long> iterable) {
                return null;
            }

            @Override
            public <S extends Plane> List<S> saveAll(Iterable<S> iterable) {
                return null;
            }

            @Override
            public void flush() {

            }

            @Override
            public <S extends Plane> S saveAndFlush(S s) {
                return null;
            }

            @Override
            public void deleteInBatch(Iterable<Plane> iterable) {

            }

            @Override
            public void deleteAllInBatch() {

            }

            @Override
            public Plane getOne(Long aLong) {
                return null;
            }

            @Override
            public <S extends Plane> List<S> findAll(Example<S> example) {
                return null;
            }

            @Override
            public <S extends Plane> List<S> findAll(Example<S> example, Sort sort) {
                return null;
            }

            @Override
            public Page<Plane> findAll(Pageable pageable) {
                return null;
            }

            @Override
            public <S extends Plane> S save(S s) {
                return null;
            }

            @Override
            public Optional<Plane> findById(Long aLong) {
                return Optional.empty();
            }

            @Override
            public boolean existsById(Long aLong) {
                return false;
            }

            @Override
            public long count() {
                return 0;
            }

            @Override
            public void deleteById(Long aLong) {

            }

            @Override
            public void delete(Plane plane) {

            }

            @Override
            public void deleteAll(Iterable<? extends Plane> iterable) {

            }

            @Override
            public void deleteAll() {

            }

            @Override
            public <S extends Plane> Optional<S> findOne(Example<S> example) {
                return Optional.empty();
            }

            @Override
            public <S extends Plane> Page<S> findAll(Example<S> example, Pageable pageable) {
                return null;
            }

            @Override
            public <S extends Plane> long count(Example<S> example) {
                return 0;
            }

            @Override
            public <S extends Plane> boolean exists(Example<S> example) {
                return false;
            }
        });
    }

    @AfterEach
    void tearDown() {
        this.prepareDatabase.deleteAll();
    }

    @Test
    void givenPlanes_whenFindPlanesAndMechanicsFromJSON_thenShouldReturnPlanesAndMechanics() {

        List<PlaneDTO> listPlanes = this.planeService.findPlanesAndMechanicsFromJSON();

        assertEquals(4, listPlanes.size());

        assertEquals(2, this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_1, listPlanes).getMechanics().size());
        assertThat(this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_1, listPlanes).getMechanics(),
                containsInAnyOrder(new MechanicDTO(
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_7).getName(),
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_7).getSurname()),
                        new MechanicDTO(
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9).getName(),
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9).getSurname())
                ));

        assertEquals(2, this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_2, listPlanes).getMechanics().size());
        assertThat(this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_2, listPlanes).getMechanics(),
                containsInAnyOrder(new MechanicDTO(
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_8).getName(),
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_8).getSurname()),
                        new MechanicDTO(
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9).getName(),
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9).getSurname())
                ));

        assertEquals(1, this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_3, listPlanes).getMechanics().size());
        assertEquals(this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_3, listPlanes).getMechanics().get(0),
                new MechanicDTO(
                        this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_8).getName(),
                        this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_8).getSurname()));

        assertTrue(isEmpty(this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_4, listPlanes).getMechanics()));

    }

    private PlaneDTO getPlaneDTOByRegistration(String registration, List<PlaneDTO> planes) {
        return planes
                .stream()
                .filter(plane -> plane.getRegistration().equals(registration))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

}
