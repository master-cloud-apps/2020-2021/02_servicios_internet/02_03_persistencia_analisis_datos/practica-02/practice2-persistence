package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.CAProvince;
import es.urjc.code.practice.models.Province;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ProvinceServiceTest {

    private ProvinceService provinceService;

    @BeforeEach
    void setUp(){
        this.provinceService = new MockServiceFactory().getMockProvinceService();
    }

    @Test
    void should_print_all_provinces_data(){
        List<Province> provinces = this.provinceService.getAllProvinces();

        assertNotNull(provinces);
        assertNotNull(provinces.get(0).getPopulationByYears().get(0));
    }

    @Test
    void should_return_all_ca_data(){
        List<CAProvince> caProvinces = this.provinceService.getCA();

        assertNotNull(caProvinces);
        CAProvince galicia =
                caProvinces.stream().filter(caProvince -> caProvince.getCA().equals("Galicia")).findFirst().orElseThrow();
        assertEquals(4, galicia.getNumProvinces());
    }
}
