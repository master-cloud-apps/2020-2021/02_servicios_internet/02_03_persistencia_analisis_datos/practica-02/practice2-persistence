package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.CabinCrewWithFlightsDTO;
import es.urjc.code.practice.dtos.CabinCrewWithTotalFlightsAndHoursDTO;
import es.urjc.code.practice.dtos.FlightDTO;
import es.urjc.code.practice.utils.EntitiesLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.text.ParseException;
import java.util.List;

import static es.urjc.code.practice.utils.EntitiesLoader.FLIGHT_CODE_1;
import static es.urjc.code.practice.utils.EntitiesLoader.FLIGHT_CODE_5;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.util.CollectionUtils.isEmpty;

@SpringBootTest
@ActiveProfiles(value = {"h2"})
class CabinCrewServiceTest {

    @Autowired
    private CabinCrewService cabinCrewService;
    @Autowired
    private EntitiesLoader prepareDatabase;

    @BeforeEach
    void setUp() throws ParseException {
        this.prepareDatabase.setUp();
    }

    @AfterEach
    void tearDown() {
        this.prepareDatabase.deleteAll();
    }

    @Test
    void givenCabinCrewWithoutHavingEmployeeCode_whenGetAllCitiesAndDates_shouldReturnNothing() {
        CabinCrewWithFlightsDTO crew =
                this.cabinCrewService.findCitiesAndDatesByCrewEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9);

        assertNull(crew);
    }

    @Test
    void givenCabinCrewWithoutFlights_whenGetAllCitiesAndDates_shouldReturnCabinCrewInfoWithEmptyFlights() {
        CabinCrewWithFlightsDTO crew = this.cabinCrewService
                .findCitiesAndDatesByCrewEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_0);

        assertEquals(this.prepareDatabase.getCabinCrew().get(this.prepareDatabase.getCabinCrew().size() - 1).getName(),
                crew.getName());
        assertEquals(this.prepareDatabase.getCabinCrew().get(this.prepareDatabase.getCabinCrew().size() - 1).getSurname(),
                crew.getSurname());
        assertTrue(isEmpty(crew.getFlights()));
    }

    @Test
    void givenCabinCrewCommander_whenGetAllCitiesAndDates_shouldReturnAllInfo() {
        CabinCrewWithFlightsDTO crew = this.cabinCrewService
                .findCitiesAndDatesByCrewEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_1);

        assertEquals(this.prepareDatabase.getCabinCrew().get(0).getName(), crew.getName());
        assertEquals(this.prepareDatabase.getCabinCrew().get(0).getSurname(), crew.getSurname());
        assertEquals(2, crew.getFlights().size());
        assertThat(crew.getFlights(), containsInAnyOrder(
                new FlightDTO(this.prepareDatabase.getFlights().get(FLIGHT_CODE_1).getOriginAirport().getCity(),
                        this.prepareDatabase.getFlights().get(FLIGHT_CODE_1).getDepartureDate()),
                new FlightDTO(this.prepareDatabase.getFlights().get(FLIGHT_CODE_5).getOriginAirport().getCity(),
                        this.prepareDatabase.getFlights().get(FLIGHT_CODE_5).getDepartureDate())
        ));
    }

    @Test
    void givenCabinCrew_whenFindAllCrewWithTotalFlights_shouldReturnAllElements() {
        List<CabinCrewWithTotalFlightsAndHoursDTO> crewWithTotalFlights =
                this.cabinCrewService.findAllCrewWithTotalFlights();

        assertEquals(this.prepareDatabase.getCabinCrew().size(), crewWithTotalFlights.size());
    }

    @Test
    void givenCabinCrew_whenFindAllCrewWithTotalFlights_shouldReturnAllInfoForEveryCabinCrew() {
        List<CabinCrewWithTotalFlightsAndHoursDTO> crewWithTotalFlights =
                this.cabinCrewService.findAllCrewWithTotalFlights();

        CabinCrewWithTotalFlightsAndHoursDTO cabinCrew = getCrewByName("Alberto", crewWithTotalFlights);
        assertEquals(2, cabinCrew.getTotalFlights());
        assertEquals(2.1f, cabinCrew.getTotalHours());

        cabinCrew = getCrewByName("Pelayo", crewWithTotalFlights);
        assertEquals(1, cabinCrew.getTotalFlights());
        assertEquals(1.05f, cabinCrew.getTotalHours());

        cabinCrew = getCrewByName("Ana", crewWithTotalFlights);
        assertEquals(1, cabinCrew.getTotalFlights());
        assertEquals(3.5f, cabinCrew.getTotalHours());

        cabinCrew = getCrewByName("Abel", crewWithTotalFlights);
        assertEquals(0, cabinCrew.getTotalFlights());
        assertEquals(0f, cabinCrew.getTotalHours());

    }

    private CabinCrewWithTotalFlightsAndHoursDTO getCrewByName(String name,
                                                               List<CabinCrewWithTotalFlightsAndHoursDTO> crewWithTotalFlights) {
        return crewWithTotalFlights.stream()
                .filter(crew -> crew.getName().equals(name))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

}
