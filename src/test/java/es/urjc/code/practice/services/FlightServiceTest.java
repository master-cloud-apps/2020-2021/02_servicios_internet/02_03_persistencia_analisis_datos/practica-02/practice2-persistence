package es.urjc.code.practice.services;

import es.urjc.code.practice.models.Flight;
import es.urjc.code.practice.utils.EntitiesLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.util.CollectionUtils.isEmpty;

@SpringBootTest
@ActiveProfiles(value = {"h2"})
class FlightServiceTest {

    @Autowired
    private FlightService flightService;
    @Autowired
    private EntitiesLoader prepareDatabase;

    @BeforeEach
    void setUp() throws ParseException {
        this.prepareDatabase.setUp();
    }

    @AfterEach
    void tearDown() {
        this.prepareDatabase.deleteAll();
    }

    @Test
    void givenNotExistsDestinationCityFlightsInPassedDate_whenGetFlightsToCity_thenShouldReturnEmptyList() {
        List<Flight> flights = this.flightService
                .findFlightsByDestinationCityInDate(
                        this.prepareDatabase.getFlights().get(EntitiesLoader.FLIGHT_CODE_1).getDestinationAirport().getCity(),
                        new Date());

        assertTrue(isEmpty(flights));
    }

    @Test
    void givenExistsFlightsInPassedDateButNotForCity_whenGetFlightsToCity_thenShouldReturnEmptyList() {
        List<Flight> flights = this.flightService
                .findFlightsByDestinationCityInDate(
                        "Utopia",
                        this.prepareDatabase.getFlights().get(EntitiesLoader.FLIGHT_CODE_1).getDepartureDate());

        assertTrue(isEmpty(flights));
    }

    @Test
    void givenExistsDestinationCityFlightsInPassedDate_whenGetFlightsToDestinationCity_thenShouldReturnFlights() {
        Flight flight = this.prepareDatabase.getFlights().get(EntitiesLoader.FLIGHT_CODE_1);
        List<Flight> flights = this.flightService
                .findFlightsByDestinationCityInDate(flight.getDestinationAirport().getCity(),
                        flight.getDepartureDate());

        assertFalse(isEmpty(flights));
        assertEquals(1, flights.size());
    }

    @Test
    void givenExistsSeveralDestinationCityFlightsInPassedDate_whenGetFlightsToDestinationCity_thenShouldReturnFlightsOrderedByHour() throws ParseException {
        Flight flight = this.prepareDatabase.getFlights().get(EntitiesLoader.FLIGHT_CODE_2);
        List<Flight> flights = this.flightService
                .findFlightsByDestinationCityInDate(flight.getDestinationAirport().getCity(),
                        flight.getDepartureDate());

        assertFalse(isEmpty(flights));
        assertEquals(3, flights.size());

        assertEquals(EntitiesLoader.CITY_3, flights.get(0).getDestinationAirport().getCity());
        assertEquals(EntitiesLoader.CITY_3, flights.get(1).getDestinationAirport().getCity());
        assertEquals(EntitiesLoader.CITY_3, flights.get(2).getDestinationAirport().getCity());

        assertTrue(this.prepareDatabase.getSdf().format(flights.get(0).getDepartureDate()).startsWith(EntitiesLoader.DEPARTURE_DATE_1));
        assertTrue(this.prepareDatabase.getSdf().format(flights.get(1).getDepartureDate()).startsWith(EntitiesLoader.DEPARTURE_DATE_1));
        assertTrue(this.prepareDatabase.getSdf().format(flights.get(2).getDepartureDate()).startsWith(EntitiesLoader.DEPARTURE_DATE_1));

        assertTrue(flights.get(0).getDepartureDate().before(flights.get(1).getDepartureDate()));
        assertTrue(flights.get(1).getDepartureDate().before(flights.get(2).getDepartureDate()));

    }

}
