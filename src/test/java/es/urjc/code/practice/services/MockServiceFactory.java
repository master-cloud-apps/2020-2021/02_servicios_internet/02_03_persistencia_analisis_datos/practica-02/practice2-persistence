package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.CAProvince;
import es.urjc.code.practice.dtos.CabinCrewWithTotalFlightsAndHoursIDTO;
import es.urjc.code.practice.dtos.FlatCabinCrewWithFlightsDTO;
import es.urjc.code.practice.dtos.FlatPlaneWithMechanicDTO;
import es.urjc.code.practice.models.CabinCrew;
import es.urjc.code.practice.models.Plane;
import es.urjc.code.practice.models.PopulationByYear;
import es.urjc.code.practice.models.Province;
import es.urjc.code.practice.repositories.CabinCrewRepository;
import es.urjc.code.practice.repositories.PlaneRepository;
import es.urjc.code.practice.repositories.ProvinceRepository;
import es.urjc.code.practice.services.impl.CabinCrewServiceImpl;
import es.urjc.code.practice.services.impl.PlaneServiceImpl;
import es.urjc.code.practice.services.impl.ProvinceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class MockServiceFactory {

    @Autowired
    private CabinCrewRepository cabinCrewRepository;
    @Autowired
    private PlaneRepository planeRepository;

    public CabinCrewService getH2CabinCrewService() {
        return new CabinCrewServiceImpl(new CabinCrewRepository() {
            @Override
            public List<FlatCabinCrewWithFlightsDTO> findByEmployeeCodeWithFlights(String employeeCode) {
                return cabinCrewRepository.findByEmployeeCodeWithFlights(employeeCode);
            }

            @Override
            public List<CabinCrewWithTotalFlightsAndHoursIDTO> findAllCrewWithTotalFlights() {
                return cabinCrewRepository.findAllCrewWithTotalFlights();
            }

            @Override
            public List<CabinCrewWithTotalFlightsAndHoursIDTO> findAllCrewWithTotalFlightsJSON() {
                return cabinCrewRepository.findAllCrewWithTotalFlights();
            }

            @Override
            public List<CabinCrew> findAll() {
                return null;
            }

            @Override
            public List<CabinCrew> findAll(Sort sort) {
                return null;
            }

            @Override
            public List<CabinCrew> findAllById(Iterable<Long> iterable) {
                return null;
            }

            @Override
            public <S extends CabinCrew> List<S> saveAll(Iterable<S> iterable) {
                return null;
            }

            @Override
            public void flush() {

            }

            @Override
            public <S extends CabinCrew> S saveAndFlush(S s) {
                return null;
            }

            @Override
            public void deleteInBatch(Iterable<CabinCrew> iterable) {

            }

            @Override
            public void deleteAllInBatch() {

            }

            @Override
            public CabinCrew getOne(Long aLong) {
                return null;
            }

            @Override
            public <S extends CabinCrew> List<S> findAll(Example<S> example) {
                return null;
            }

            @Override
            public <S extends CabinCrew> List<S> findAll(Example<S> example, Sort sort) {
                return null;
            }

            @Override
            public Page<CabinCrew> findAll(Pageable pageable) {
                return null;
            }

            @Override
            public <S extends CabinCrew> S save(S s) {
                return null;
            }

            @Override
            public Optional<CabinCrew> findById(Long aLong) {
                return Optional.empty();
            }

            @Override
            public boolean existsById(Long aLong) {
                return false;
            }

            @Override
            public long count() {
                return 0;
            }

            @Override
            public void deleteById(Long aLong) {

            }

            @Override
            public void delete(CabinCrew cabinCrew) {

            }

            @Override
            public void deleteAll(Iterable<? extends CabinCrew> iterable) {

            }

            @Override
            public void deleteAll() {

            }

            @Override
            public <S extends CabinCrew> Optional<S> findOne(Example<S> example) {
                return Optional.empty();
            }

            @Override
            public <S extends CabinCrew> Page<S> findAll(Example<S> example, Pageable pageable) {
                return null;
            }

            @Override
            public <S extends CabinCrew> long count(Example<S> example) {
                return 0;
            }

            @Override
            public <S extends CabinCrew> boolean exists(Example<S> example) {
                return false;
            }
        });
    }

    public PlaneService getH2PlaneService() {
        return new PlaneServiceImpl(new PlaneRepository() {
            @Override
            public List<FlatPlaneWithMechanicDTO> findPlaneMechanics() {
                return planeRepository.findPlaneMechanics();
            }

            @Override
            public List<FlatPlaneWithMechanicDTO> findPlaneMechanicsJSON() {
                return planeRepository.findPlaneMechanics();
            }

            @Override
            public List<Plane> findAll() {
                return null;
            }

            @Override
            public List<Plane> findAll(Sort sort) {
                return null;
            }

            @Override
            public List<Plane> findAllById(Iterable<Long> iterable) {
                return null;
            }

            @Override
            public <S extends Plane> List<S> saveAll(Iterable<S> iterable) {
                return null;
            }

            @Override
            public void flush() {

            }

            @Override
            public <S extends Plane> S saveAndFlush(S s) {
                return null;
            }

            @Override
            public void deleteInBatch(Iterable<Plane> iterable) {

            }

            @Override
            public void deleteAllInBatch() {

            }

            @Override
            public Plane getOne(Long aLong) {
                return null;
            }

            @Override
            public <S extends Plane> List<S> findAll(Example<S> example) {
                return null;
            }

            @Override
            public <S extends Plane> List<S> findAll(Example<S> example, Sort sort) {
                return null;
            }

            @Override
            public Page<Plane> findAll(Pageable pageable) {
                return null;
            }

            @Override
            public <S extends Plane> S save(S s) {
                return null;
            }

            @Override
            public Optional<Plane> findById(Long aLong) {
                return Optional.empty();
            }

            @Override
            public boolean existsById(Long aLong) {
                return false;
            }

            @Override
            public long count() {
                return 0;
            }

            @Override
            public void deleteById(Long aLong) {

            }

            @Override
            public void delete(Plane plane) {

            }

            @Override
            public void deleteAll(Iterable<? extends Plane> iterable) {

            }

            @Override
            public void deleteAll() {

            }

            @Override
            public <S extends Plane> Optional<S> findOne(Example<S> example) {
                return Optional.empty();
            }

            @Override
            public <S extends Plane> Page<S> findAll(Example<S> example, Pageable pageable) {
                return null;
            }

            @Override
            public <S extends Plane> long count(Example<S> example) {
                return 0;
            }

            @Override
            public <S extends Plane> boolean exists(Example<S> example) {
                return false;
            }
        });
    }

    public ProvinceService getMockProvinceService() {
        return new ProvinceServiceImpl(new ProvinceRepository() {
            @Override
            public <S extends Province> S save(S s) {
                return null;
            }

            @Override
            public <S extends Province> List<S> saveAll(Iterable<S> iterable) {
                return null;
            }

            @Override
            public Optional<Province> findById(String s) {
                return Optional.empty();
            }

            @Override
            public boolean existsById(String s) {
                return false;
            }

            @Override
            public List<Province> findAll() {
                List<PopulationByYear> popAlbacete = Arrays.asList(new PopulationByYear());
                List<PopulationByYear> popPontevedra = Arrays.asList(new PopulationByYear());
                return Arrays.asList(Province.builder().provinceName("Albacete").populationByYears(popAlbacete).build(),
                        Province.builder().populationByYears(popPontevedra).provinceName("Pontevedra").build());
            }

            @Override
            public Iterable<Province> findAllById(Iterable<String> iterable) {
                return null;
            }

            @Override
            public long count() {
                return 0;
            }

            @Override
            public void deleteById(String s) {

            }

            @Override
            public void delete(Province province) {

            }

            @Override
            public void deleteAll(Iterable<? extends Province> iterable) {

            }

            @Override
            public void deleteAll() {

            }

            @Override
            public List<Province> findAll(Sort sort) {
                return null;
            }

            @Override
            public Page<Province> findAll(Pageable pageable) {
                return null;
            }

            @Override
            public <S extends Province> S insert(S s) {
                return null;
            }

            @Override
            public <S extends Province> List<S> insert(Iterable<S> iterable) {
                return null;
            }

            @Override
            public <S extends Province> Optional<S> findOne(Example<S> example) {
                return Optional.empty();
            }

            @Override
            public <S extends Province> List<S> findAll(Example<S> example) {
                return null;
            }

            @Override
            public <S extends Province> List<S> findAll(Example<S> example, Sort sort) {
                return null;
            }

            @Override
            public <S extends Province> Page<S> findAll(Example<S> example, Pageable pageable) {
                return null;
            }

            @Override
            public <S extends Province> long count(Example<S> example) {
                return 0;
            }

            @Override
            public <S extends Province> boolean exists(Example<S> example) {
                return false;
            }

            @Override
            public List<CAProvince> getCA() {
                return Arrays.asList(CAProvince.builder().numProvinces(4).CA("Galicia").build());
            }
        });
    }
}
