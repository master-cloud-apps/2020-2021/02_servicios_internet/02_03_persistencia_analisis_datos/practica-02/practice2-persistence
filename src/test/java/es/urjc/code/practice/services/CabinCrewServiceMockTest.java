package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.*;
import es.urjc.code.practice.models.CabinCrew;
import es.urjc.code.practice.repositories.CabinCrewRepository;
import es.urjc.code.practice.services.impl.CabinCrewServiceImpl;
import es.urjc.code.practice.utils.EntitiesLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import static es.urjc.code.practice.utils.EntitiesLoader.FLIGHT_CODE_1;
import static es.urjc.code.practice.utils.EntitiesLoader.FLIGHT_CODE_5;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.util.CollectionUtils.isEmpty;

@SpringBootTest
@ActiveProfiles(value = {"h2"})
class CabinCrewServiceMockTest {

    private CabinCrewService cabinCrewService;
    @Autowired
    private CabinCrewRepository cabinCrewRepository;
    @Autowired
    private EntitiesLoader prepareDatabase;

    @BeforeEach
    void setUp() throws ParseException {
        this.prepareDatabase.setUp();
        this.cabinCrewService = new CabinCrewServiceImpl(new CabinCrewRepository() {
            @Override
            public List<FlatCabinCrewWithFlightsDTO> findByEmployeeCodeWithFlights(String employeeCode) {
                return cabinCrewRepository.findByEmployeeCodeWithFlights(employeeCode);
            }

            @Override
            public List<CabinCrewWithTotalFlightsAndHoursIDTO> findAllCrewWithTotalFlights() {
                return cabinCrewRepository.findAllCrewWithTotalFlights();
            }

            @Override
            public List<CabinCrewWithTotalFlightsAndHoursIDTO> findAllCrewWithTotalFlightsJSON() {
                return cabinCrewRepository.findAllCrewWithTotalFlights();
            }

            @Override
            public List<CabinCrew> findAll() {
                return null;
            }

            @Override
            public List<CabinCrew> findAll(Sort sort) {
                return null;
            }

            @Override
            public List<CabinCrew> findAllById(Iterable<Long> iterable) {
                return null;
            }

            @Override
            public <S extends CabinCrew> List<S> saveAll(Iterable<S> iterable) {
                return null;
            }

            @Override
            public void flush() {

            }

            @Override
            public <S extends CabinCrew> S saveAndFlush(S s) {
                return null;
            }

            @Override
            public void deleteInBatch(Iterable<CabinCrew> iterable) {

            }

            @Override
            public void deleteAllInBatch() {

            }

            @Override
            public CabinCrew getOne(Long aLong) {
                return null;
            }

            @Override
            public <S extends CabinCrew> List<S> findAll(Example<S> example) {
                return null;
            }

            @Override
            public <S extends CabinCrew> List<S> findAll(Example<S> example, Sort sort) {
                return null;
            }

            @Override
            public Page<CabinCrew> findAll(Pageable pageable) {
                return null;
            }

            @Override
            public <S extends CabinCrew> S save(S s) {
                return null;
            }

            @Override
            public Optional<CabinCrew> findById(Long aLong) {
                return Optional.empty();
            }

            @Override
            public boolean existsById(Long aLong) {
                return false;
            }

            @Override
            public long count() {
                return 0;
            }

            @Override
            public void deleteById(Long aLong) {

            }

            @Override
            public void delete(CabinCrew cabinCrew) {

            }

            @Override
            public void deleteAll(Iterable<? extends CabinCrew> iterable) {

            }

            @Override
            public void deleteAll() {

            }

            @Override
            public <S extends CabinCrew> Optional<S> findOne(Example<S> example) {
                return Optional.empty();
            }

            @Override
            public <S extends CabinCrew> Page<S> findAll(Example<S> example, Pageable pageable) {
                return null;
            }

            @Override
            public <S extends CabinCrew> long count(Example<S> example) {
                return 0;
            }

            @Override
            public <S extends CabinCrew> boolean exists(Example<S> example) {
                return false;
            }
        });
    }

    @AfterEach
    void tearDown() {
        this.prepareDatabase.deleteAll();
    }

    @Test
    void givenCabinCrewWithoutHavingEmployeeCode_whenGetAllCitiesAndDates_shouldReturnNothing() {
        CabinCrewWithFlightsDTO crew =
                this.cabinCrewService.findCitiesAndDatesByCrewEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9);

        assertNull(crew);
    }

    @Test
    void givenCabinCrewWithoutFlights_whenGetAllCitiesAndDates_shouldReturnCabinCrewInfoWithEmptyFlights() {
        CabinCrewWithFlightsDTO crew = this.cabinCrewService
                .findCitiesAndDatesByCrewEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_0);

        assertEquals(this.prepareDatabase.getCabinCrew().get(this.prepareDatabase.getCabinCrew().size() - 1).getName(),
                crew.getName());
        assertEquals(this.prepareDatabase.getCabinCrew().get(this.prepareDatabase.getCabinCrew().size() - 1).getSurname(),
                crew.getSurname());
        assertTrue(isEmpty(crew.getFlights()));
    }

    @Test
    void givenCabinCrewCommander_whenGetAllCitiesAndDates_shouldReturnAllInfo() {
        CabinCrewWithFlightsDTO crew = this.cabinCrewService
                .findCitiesAndDatesByCrewEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_1);

        assertEquals(this.prepareDatabase.getCabinCrew().get(0).getName(), crew.getName());
        assertEquals(this.prepareDatabase.getCabinCrew().get(0).getSurname(), crew.getSurname());
        assertEquals(2, crew.getFlights().size());
        assertThat(crew.getFlights(), containsInAnyOrder(
                new FlightDTO(this.prepareDatabase.getFlights().get(FLIGHT_CODE_1).getOriginAirport().getCity(),
                        this.prepareDatabase.getFlights().get(FLIGHT_CODE_1).getDepartureDate()),
                new FlightDTO(this.prepareDatabase.getFlights().get(FLIGHT_CODE_5).getOriginAirport().getCity(),
                        this.prepareDatabase.getFlights().get(FLIGHT_CODE_5).getDepartureDate())
        ));
    }

    @Test
    void givenCabinCrew_whenFindAllCrewWithTotalFlights_shouldReturnAllElements() {
        List<CabinCrewWithTotalFlightsAndHoursDTO> crewWithTotalFlights =
                this.cabinCrewService.findAllCrewWithTotalFlights();

        assertEquals(this.prepareDatabase.getCabinCrew().size(), crewWithTotalFlights.size());
    }

    @Test
    void givenCabinCrew_whenFindAllCrewWithTotalFlights_shouldReturnAllInfoForEveryCabinCrew() {
        List<CabinCrewWithTotalFlightsAndHoursDTO> crewWithTotalFlights =
                this.cabinCrewService.findAllCrewWithTotalFlights();

        CabinCrewWithTotalFlightsAndHoursDTO cabinCrew = getCrewByName("Alberto", crewWithTotalFlights);
        assertEquals(2, cabinCrew.getTotalFlights());
        assertEquals(2.1f, cabinCrew.getTotalHours());

        cabinCrew = getCrewByName("Pelayo", crewWithTotalFlights);
        assertEquals(1, cabinCrew.getTotalFlights());
        assertEquals(1.05f, cabinCrew.getTotalHours());

        cabinCrew = getCrewByName("Ana", crewWithTotalFlights);
        assertEquals(1, cabinCrew.getTotalFlights());
        assertEquals(3.5f, cabinCrew.getTotalHours());

        cabinCrew = getCrewByName("Abel", crewWithTotalFlights);
        assertEquals(0, cabinCrew.getTotalFlights());
        assertEquals(0f, cabinCrew.getTotalHours());

    }

    @Test
    void givenCabinCrew_whenFindAllCrewWithTotalFlightsJSON_shouldReturnAllElements() {
        List<CabinCrewWithTotalFlightsAndHoursDTO> crewWithTotalFlights =
                this.cabinCrewService.findAllCrewWithTotalFlightsFromJSON();

        assertEquals(this.prepareDatabase.getCabinCrew().size(), crewWithTotalFlights.size());
    }

    @Test
    void givenCabinCrew_whenFindAllCrewWithTotalFlightsJSON_shouldReturnAllInfoForEveryCabinCrew() {
        List<CabinCrewWithTotalFlightsAndHoursDTO> crewWithTotalFlights =
                this.cabinCrewService.findAllCrewWithTotalFlightsFromJSON();

        CabinCrewWithTotalFlightsAndHoursDTO cabinCrew = getCrewByName("Alberto", crewWithTotalFlights);
        assertEquals(2, cabinCrew.getTotalFlights());
        assertEquals(2.1f, cabinCrew.getTotalHours());

        cabinCrew = getCrewByName("Pelayo", crewWithTotalFlights);
        assertEquals(1, cabinCrew.getTotalFlights());
        assertEquals(1.05f, cabinCrew.getTotalHours());

        cabinCrew = getCrewByName("Ana", crewWithTotalFlights);
        assertEquals(1, cabinCrew.getTotalFlights());
        assertEquals(3.5f, cabinCrew.getTotalHours());

        cabinCrew = getCrewByName("Abel", crewWithTotalFlights);
        assertEquals(0, cabinCrew.getTotalFlights());
        assertEquals(0f, cabinCrew.getTotalHours());

    }

    private CabinCrewWithTotalFlightsAndHoursDTO getCrewByName(String name,
                                                               List<CabinCrewWithTotalFlightsAndHoursDTO> crewWithTotalFlights) {
        return crewWithTotalFlights.stream()
                .filter(crew -> crew.getName().equals(name))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

}
