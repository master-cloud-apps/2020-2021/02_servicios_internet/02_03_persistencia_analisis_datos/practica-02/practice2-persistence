package es.urjc.code.practice;

import es.urjc.code.practice.services.FlightService;
import es.urjc.code.practice.services.LoggerService;
import es.urjc.code.practice.services.PlaneService;
import es.urjc.code.practice.utils.EntitiesLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles(value = {"h2", "practice1"})
class Practice1RunnerTest {

    @Autowired
    private Practice1Runner practice1Runner;

    @Autowired
    private PlaneService planeService;

    @Autowired
    private EntitiesLoader prepareDatabase;
    @Autowired
    private FlightService flightService;
    @Autowired
    private LoggerService loggerService;

    @BeforeEach
    void setUp() throws ParseException {
        this.prepareDatabase.setUp();
    }

    @AfterEach
    void tearDown() {
        this.prepareDatabase.deleteAll();
    }

    @Test
    void should_execute_runner() throws Exception {
        this.practice1Runner.run();
        assertNotNull(this.planeService.findPlanesAndMechanics());
    }

    @Test
    void logOnlyOneList(){
        this.loggerService.logElements("Showing flight by destination city and departure date sorted by hours",
                () -> this.flightService
                        .findFlightsByDestinationCityInDate(EntitiesLoader.CITY_3,
                                this.prepareDatabase.getSdf().parse(EntitiesLoader.DEPARTURE_DATE_1_WITH_HOUR)));
    }

}
