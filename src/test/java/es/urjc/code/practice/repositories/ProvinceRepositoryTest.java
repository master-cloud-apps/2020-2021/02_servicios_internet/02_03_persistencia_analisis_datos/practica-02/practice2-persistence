package es.urjc.code.practice.repositories;

import es.urjc.code.practice.dtos.CAProvince;
import es.urjc.code.practice.models.Province;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled
@SpringBootTest
@ActiveProfiles(value = {"h2"})
class ProvinceRepositoryTest {

    @Autowired
    private ProvinceRepository provinceRepository;

    @Test
    void should_get_all_data(){
        List<Province> listProvincias = this.provinceRepository.findAll();

        assertNotNull(listProvincias);
        assertNotNull(listProvincias.get(0));
        assertNotNull(listProvincias.get(0).getPopulationByYears().get(0));

        listProvincias.get(0).getPopulationByYears().stream().limit(10).forEach(System.out::println);
    }

    @Test
    void should_get_ca_provinces(){
        List<CAProvince> caProvinces = this.provinceRepository.getCA();

        assertNotNull(caProvinces);
        caProvinces.stream().forEach(System.out::println);
    }
}
