db.provincia.aggregate([
		{ $match: { CA: "Andalucía" } },
		{ $project: { Nombre: 1, CA: 1, Superfice: 1, _id: 0 } },]).pretty();

// Apartado 2
db.provincia.aggregate([
  {$sort: {Nombre: 1}},
  {$group: {_id: "$CA", provinciasNum: {$sum: 1}}},
  {$project: {_id: {$ifNull: ["$_id", "sin comunidad"]}, provinciasNum: 1}}
]).pretty()