package es.urjc.code.practice.repositories;

import es.urjc.code.practice.models.Airport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AirportRepository extends JpaRepository<Airport, Long> {
}
