package es.urjc.code.practice.dtos;

import lombok.Getter;

import java.util.Date;

@Getter
public class FlatCabinCrewWithFlightsDTO {

    private String name;
    private String surname;
    private String originCity;
    private Date departureDate;

    public FlatCabinCrewWithFlightsDTO(String name, String surname, String originCity, Date departureDate) {
        this.name = name;
        this.surname = surname;
        this.originCity = originCity;
        this.departureDate = departureDate;
    }
}
