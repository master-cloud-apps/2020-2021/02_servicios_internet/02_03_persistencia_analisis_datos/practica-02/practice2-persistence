package es.urjc.code.practice.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class CabinCrewWithTotalFlightsAndHoursDTO {

    private String name;
    private String surname;
    private Long totalFlights;
    private Float totalHours;

    public CabinCrewWithTotalFlightsAndHoursDTO(CabinCrewWithTotalFlightsAndHoursIDTO iCabinCrew) {
        this.name = iCabinCrew.getName();
        this.surname = iCabinCrew.getSurname();
        this.totalFlights = iCabinCrew.getTotalFlights();
        this.totalHours = iCabinCrew.getTotalHours() == null ? 0 : iCabinCrew.getTotalHours();
    }

}
