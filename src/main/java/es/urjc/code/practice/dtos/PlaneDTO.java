package es.urjc.code.practice.dtos;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
public class PlaneDTO {

    private Long id;
    private String registration;
    private String producer;
    private String model;
    private Float flightHours;

    private List<MechanicDTO> mechanics = new ArrayList<>();

    public PlaneDTO(FlatPlaneWithMechanicDTO flatPlane) {
        this.id = flatPlane.getId();
        this.registration = flatPlane.getRegistration();
        this.producer = flatPlane.getProducer();
        this.model = flatPlane.getModel();
        this.flightHours = flatPlane.getFlightHours();
        if (flatPlane.getMechanic() != null)
            this.mechanics.add(flatPlane.getMechanic());
    }

}
