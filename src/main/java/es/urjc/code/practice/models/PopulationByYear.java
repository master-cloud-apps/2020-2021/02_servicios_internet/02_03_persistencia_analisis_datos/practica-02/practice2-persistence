package es.urjc.code.practice.models;

import lombok.Getter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@ToString
public class PopulationByYear {
    @Field("Anyo")
    private int year;
    @Field("Valor")
    private Integer value;

}
