package es.urjc.code.practice.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class Plane {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, nullable = false)
    private String registration;

    @NotBlank
    private String producer;

    @NotBlank
    private String model;

    private Float flightHours;

    @Column(columnDefinition="json")
    private String maintenanceInfo;

}
