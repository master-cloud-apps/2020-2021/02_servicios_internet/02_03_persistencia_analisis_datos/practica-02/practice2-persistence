package es.urjc.code.practice.models;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Getter
@Setter
@ToString(exclude = {"id"})
@Document("provincia")
@Builder
public class Province {

    @Id
    private String id;
    @Field("Nombre")
    private String provinceName;
    @Field("CA")
    private String autonomyName;
    @Field("Superficie")
    private Integer surface;
    @Field("Datos")
    private List<PopulationByYear> populationByYears;
}
