package es.urjc.code.practice.models;

import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@NoArgsConstructor
public class CabinCrew extends Employee {

    @NotBlank
    private String position;

    @OneToMany(mappedBy = "cabinCrew", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FlightCabinCrew> flights;

    public CabinCrew(Long id, String employeeCode, String name, String surname, String companyName, String position) {
        super(id, employeeCode, name, surname, companyName);
        this.position = position;
    }

}
