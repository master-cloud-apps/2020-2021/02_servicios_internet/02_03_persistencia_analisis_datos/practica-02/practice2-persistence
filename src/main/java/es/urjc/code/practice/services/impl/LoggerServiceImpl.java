package es.urjc.code.practice.services.impl;

import es.urjc.code.practice.services.EntitiesFinder;
import es.urjc.code.practice.services.LogElement;
import es.urjc.code.practice.services.LogElements;
import es.urjc.code.practice.services.LoggerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public class LoggerServiceImpl implements LoggerService {

    private static final Logger logger = LogManager.getLogger(LoggerServiceImpl.class);

    private final LogElements logElements =
            (entitiesFinder -> entitiesFinder.findObjects().forEach(logger::info));

    public void logElements(String head, EntitiesFinder entitiesFinder) {
        this.logElement(head, () -> {
            try {
                this.logElements.logElementsAfterFindThem(entitiesFinder);
            } catch (ParseException e) {
                logger.error(e);
            }
        });
    }

    public void logOneElement(String head, Object elementToLog) {
        this.logElement(head, () -> logger.info(elementToLog));
    }

    public void logElement(String head, LogElement consumer) {
        logger.info(head);
        logger.info("------------------------------------------------------");
        consumer.logElement();
        logger.info("------------------------------------------------------\n\n");
    }
}
