package es.urjc.code.practice.services;

import es.urjc.code.practice.models.Flight;

import java.util.Date;
import java.util.List;

public interface FlightService {

    List<Flight> findFlightsByDestinationCityInDate(String city, Date date);

}
