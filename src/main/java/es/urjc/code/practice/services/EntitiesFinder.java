package es.urjc.code.practice.services;

import java.text.ParseException;
import java.util.List;

public interface EntitiesFinder {

    List findObjects() throws ParseException;
}
