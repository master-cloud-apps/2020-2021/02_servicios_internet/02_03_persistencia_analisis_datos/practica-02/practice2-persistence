package es.urjc.code.practice.services.impl;

import es.urjc.code.practice.models.Flight;
import es.urjc.code.practice.repositories.FlightRepository;
import es.urjc.code.practice.services.FlightService;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {

    private FlightRepository flightRepository;

    public FlightServiceImpl(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    @Override
    public List<Flight> findFlightsByDestinationCityInDate(String city, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        return this.flightRepository.findFlightsByDestinationCityAndDate(city, sdf.format(date));
    }

}
