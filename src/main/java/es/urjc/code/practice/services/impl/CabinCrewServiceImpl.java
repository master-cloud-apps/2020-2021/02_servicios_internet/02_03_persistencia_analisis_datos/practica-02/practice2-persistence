package es.urjc.code.practice.services.impl;

import es.urjc.code.practice.dtos.CabinCrewWithFlightsDTO;
import es.urjc.code.practice.dtos.CabinCrewWithTotalFlightsAndHoursDTO;
import es.urjc.code.practice.dtos.FlatCabinCrewWithFlightsDTO;
import es.urjc.code.practice.dtos.FlightDTO;
import es.urjc.code.practice.repositories.CabinCrewRepository;
import es.urjc.code.practice.services.CabinCrewService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;


@Service
public class CabinCrewServiceImpl implements CabinCrewService {

    private CabinCrewRepository cabinCrewRepository;

    public CabinCrewServiceImpl(CabinCrewRepository cabinCrewRepository) {
        this.cabinCrewRepository = cabinCrewRepository;
    }

    @Override
    public CabinCrewWithFlightsDTO findCitiesAndDatesByCrewEmployeeCode(String employeeCode) {
        CabinCrewWithFlightsDTO cabinCrewWithFlightsDTO = null;
        List<FlatCabinCrewWithFlightsDTO> flatCabinCrewWithFlightsDTOS =
                this.cabinCrewRepository.findByEmployeeCodeWithFlights(employeeCode);
        if (!isEmpty(flatCabinCrewWithFlightsDTOS)) {
            cabinCrewWithFlightsDTO = new CabinCrewWithFlightsDTO(
                    flatCabinCrewWithFlightsDTOS.get(0).getName(),
                    flatCabinCrewWithFlightsDTOS.get(0).getSurname(),
                    flatCabinCrewWithFlightsDTOS
                            .stream()
                            .filter(flatCabinCrewWithFlightsDTO -> flatCabinCrewWithFlightsDTO.getOriginCity() != null)
                            .map(flatCabinCrewWithFlightsDTO -> new FlightDTO(flatCabinCrewWithFlightsDTO.getOriginCity(),
                                    flatCabinCrewWithFlightsDTO.getDepartureDate()))
                            .collect(Collectors.toList())
            );
        }
        return cabinCrewWithFlightsDTO;
    }

    @Override
    public List<CabinCrewWithTotalFlightsAndHoursDTO> findAllCrewWithTotalFlights() {
        return this.cabinCrewRepository.findAllCrewWithTotalFlights()
                .stream()
                .map(CabinCrewWithTotalFlightsAndHoursDTO::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<CabinCrewWithTotalFlightsAndHoursDTO> findAllCrewWithTotalFlightsFromJSON() {
        return this.cabinCrewRepository.findAllCrewWithTotalFlightsJSON()
                .stream()
                .map(CabinCrewWithTotalFlightsAndHoursDTO::new)
                .collect(Collectors.toList());
    }

}
