package es.urjc.code.practice;

import es.urjc.code.practice.services.LoggerService;
import es.urjc.code.practice.services.ProvinceService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;

@Controller
@Profile("default")
public class DataLogger implements CommandLineRunner {
    private final ProvinceService provinceService;
    private final LoggerService loggerService;

    public DataLogger(ProvinceService provinceService, LoggerService loggerService) {
        this.provinceService = provinceService;
        this.loggerService = loggerService;
    }

    @Override
    public void run(String... args) {
        this.loggerService.logElements("All elements from provincia collection", this.provinceService::getAllProvinces);
        this.loggerService.logElements("All CAs with their number of provinces", this.provinceService::getCA);
    }
}
