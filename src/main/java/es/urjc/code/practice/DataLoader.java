package es.urjc.code.practice;

import es.urjc.code.practice.services.CabinCrewService;
import es.urjc.code.practice.services.LoggerService;
import es.urjc.code.practice.services.PlaneService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;


@Controller
@Profile("default")
public class DataLoader implements CommandLineRunner {

    private final PlaneService planeService;
    private final CabinCrewService cabinCrewService;
    private final LoggerService loggerService;

    public DataLoader(PlaneService planeService,
                      CabinCrewService cabinCrewService,
                      LoggerService loggerService) {
        this.planeService = planeService;
        this.cabinCrewService = cabinCrewService;
        this.loggerService = loggerService;
    }

    @Override
    public void run(String... args) {
        this.loggerService.logElements("Showing airplanes with their mechanics from JSON info",
                this.planeService::findPlanesAndMechanicsFromJSON);

        this.loggerService.logElements("Showing all cabin crew with their total number of flights and their total " +
                        "flight hours from JSON info",
                this.cabinCrewService::findAllCrewWithTotalFlightsFromJSON);
    }


}
